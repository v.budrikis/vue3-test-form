import { createStore } from 'vuex';
import axios from 'axios';
import { FormInterface } from '@/interfaces';

export default createStore({
  state: {
    forms: [] as FormInterface[],
  },
  mutations: {
    UPDATE_FORMS(state, data: FormInterface[]) {
      state.forms = data;
    },
  },
  actions: {
    async GET_FORMS({ commit }) {
      try {
        await axios.get<{forms: FormInterface[]}>('/forms.json')
          .then((res) => {
            commit('UPDATE_FORMS', res.data.forms);
          });
      } catch (e) {
        console.log(e);
      }
    },
  },
  getters: {
    forms: (state) => state.forms,
    getFormById: (state) => (val: string) => state.forms.find((form) => form.id === val),
  },
  modules: {},
});
