import { FieldInterface } from './Field.interface';

export interface FormInterface {
    id: string
    fields: FieldInterface[]
}
