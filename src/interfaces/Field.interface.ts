import { FieldTypesType } from './FieldTypes.interface';
import { OptionInterface } from './Option.interface';

export interface FieldInterface {
    id: string
    name: string
    type: FieldTypesType
    required?: boolean
    validation?: boolean
    options?: OptionInterface[]
}
